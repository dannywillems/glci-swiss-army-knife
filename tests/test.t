No CI file:
  $ ../main.exe merge
  Error
    Error:
      Not an existing file: '.gitlab-ci.yml'
  
  [1]

Phony CI file:
  $ touch .gitlab-ci.yml
  $ ../main.exe merge
  Error
    Error:
      Invalid GitLab CI file .gitlab-ci.yml
  
  [1]
  $ rm .gitlab-ci.yml

Working default CI file:
  $ cp ../test_confs/small/.gitlab-ci-small.yml .gitlab-ci.yml
  $ ../main.exe merge
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    script:
    - scripts/check_opam_test.sh
  $ rm .gitlab-ci.yml
