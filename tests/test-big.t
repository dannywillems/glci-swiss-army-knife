Big tests:
  $ ../main.exe --ci-file ../test_confs/from_tezos_master/.gitlab-ci.yml lint
  `Assoc ([("status", `String ("valid")); ("errors", `List ([]));
            ("warnings", `List ([]))])
  $ ../main.exe --ci-file ../test_confs/from_tezos_master/.gitlab-ci.yml focus sanity
  variables:
    build_deps_image_version: 4eb9728016e05758054c600ddc66c7e295c27a26
    build_deps_image_name: registry.gitlab.com/tezos/opam-repository
    public_docker_image_name: docker.io/${CI_PROJECT_PATH}
    GIT_STRATEGY: fetch
    GIT_DEPTH: 1
    GET_SOURCES_ATTEMPTS: 2
    ARTIFACT_DOWNLOAD_ATTEMPTS: 2
  stages:
  - sanity
  - build
  - sanity_ci
  - test
  - doc
  - packaging
  - build_release
  - publish_release
  - test_coverage
  - publish_coverage
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: sanity
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    script:
    - if [ "${build_deps_image_version}" != "${opam_repository_tag}" ] ; then echo "Inconsistent
      dependencies hash between 'scripts/version.sh' and '.gitlab-ci.yml'." ; echo "${build_deps_image_version}
      != ${opam_repository_tag}" ; exit 1 ; fi
    - scripts/check_opam_test.sh
    - diff tests_python/pyproject.toml /home/tezos/pyproject.toml
    interruptible: true
