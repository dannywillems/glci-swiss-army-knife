Small tests:
  $ ../main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml merge
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    script:
    - scripts/check_opam_test.sh
  $ ../main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml lint
  `Assoc ([("status", `String ("valid")); ("errors", `List ([]));
            ("warnings", `List ([]))])
  $ ../main.exe --ci-file ../test_confs/small/.gitlab-ci-small.yml focus sanity
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    script:
    - scripts/check_opam_test.sh
