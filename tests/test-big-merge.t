
Big tests:
  $ ../main.exe --ci-file ../test_confs/from_tezos_master/.gitlab-ci.yml merge
  variables:
    build_deps_image_version: 4eb9728016e05758054c600ddc66c7e295c27a26
    build_deps_image_name: registry.gitlab.com/tezos/opam-repository
    public_docker_image_name: docker.io/${CI_PROJECT_PATH}
    GIT_STRATEGY: fetch
    GIT_DEPTH: 1
    GET_SOURCES_ATTEMPTS: 2
    ARTIFACT_DOWNLOAD_ATTEMPTS: 2
  stages:
  - sanity
  - build
  - sanity_ci
  - test
  - doc
  - packaging
  - build_release
  - publish_release
  - test_coverage
  - publish_coverage
  sanity:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: sanity
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    script:
    - if [ "${build_deps_image_version}" != "${opam_repository_tag}" ] ; then echo "Inconsistent
      dependencies hash between 'scripts/version.sh' and '.gitlab-ci.yml'." ; echo "${build_deps_image_version}
      != ${opam_repository_tag}" ; exit 1 ; fi
    - scripts/check_opam_test.sh
    - diff tests_python/pyproject.toml /home/tezos/pyproject.toml
    interruptible: true
  check_opam_deps:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    script:
    - ./scripts/opam-check.sh
  check_opam_lint:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    script:
    - find . ! -path "./_opam/*" -name "*.opam" -exec opam lint {} +;
  check_linting:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    script:
    - make check-linting
  check_precommit_hook:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    script:
    - ./scripts/pre_commit/pre_commit.py --test-itself
    - cd tests_python
    - poetry run pylint ../scripts/pre_commit/pre_commit.py
    - poetry run pycodestyle ../scripts/pre_commit/pre_commit.py
    - poetry run mypy ../scripts/pre_commit/pre_commit.py
  check_python_linting:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    script:
    - make check-python-linting
  check_python_types:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    script:
    - make -C tests_python typecheck
  check_scripts_b58_prefix:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    before_script:
    - cd scripts/b58_prefix
    - poetry install
    script:
    - poetry run pylint b58_prefix.py --disable=missing-docstring --disable=invalid-name
    - poetry run pytest test_b58_prefix.py -v
  build:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    script:
    - . ./scripts/version.sh
    - dune build @runtest_dune_template
    - make all build-test
    - opam clean
    cache:
      key: $CI_COMMIT_REF_SLUG
      paths:
      - _build
      - tezos-*
    artifacts:
      name: build-$CI_COMMIT_REF_SLUG
      paths:
      - tezos-*
      - src/proto_*/parameters/*.json
      - _build/default/src/lib_protocol_compiler/main_native.exe
      expire_in: 1 day
      when: on_success
  build_arm64:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    when: manual
    script:
    - . ./scripts/version.sh
    - dune build @all
    - dune install --prefix install_root
    artifacts:
      name: build-$CI_COMMIT_REF_SLUG
      paths:
      - install_root/bin/tezos-*
    tags:
    - arm64
  build_generate_local_gitlab_ci_file:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    script:
    - opam repository add default https://opam.ocaml.org -y
    - opam update
    - opam install yaml -y
    - dune exec scripts/generate-local-gitlab-ci/generate_local_gitlab_ci.exe build_generate_local_gitlab_ci_file
  sanity_ci:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: sanity_ci
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    script:
    - src/tooling/lint.sh --check-gitlab-ci-yml
    interruptible: true
  test-script-gen-genesis:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: test
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - cd scripts/gen-genesis
    script:
    - dune build gen_genesis.exe
    interruptible: true
  test-static-libs-patch:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: test
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    script:
    - git apply packaging/build/static_libs.patch
  unit:alltest:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    artifacts:
      name: alltest-${CI_COMMIT_SHA}
      paths:
      - test_results
      expire_in: 1 day
    script:
    - scripts/test_wrapper.sh src/bin_client client
    - scripts/test_wrapper.sh src/bin_snoop/latex snoop_latex
    - scripts/test_wrapper.sh src/lib_base base
    - scripts/test_wrapper.sh src/lib_benchmark/lib_micheline_rewriting benchmark_lib_micheline_rewriting
    - scripts/test_wrapper.sh src/lib_benchmark benchmark
    - scripts/test_wrapper.sh src/lib_clic clic
    - scripts/test_wrapper.sh src/lib_client_base client_base
    - scripts/test_wrapper.sh src/lib_crypto crypto
    - scripts/test_wrapper.sh src/lib_error_monad error_monad
    - scripts/test_wrapper.sh src/lib_lwt_result_stdlib lwt_result_stdlib
    - scripts/test_wrapper.sh src/lib_micheline micheline
    - scripts/test_wrapper.sh src/lib_p2p p2p
    - scripts/test_wrapper.sh src/lib_protocol_compiler protocol_compiler
    - scripts/test_wrapper.sh src/lib_protocol_environment protocol_environment
    - scripts/test_wrapper.sh src/lib_proxy proxy
    - scripts/test_wrapper.sh src/lib_requester requester
    - scripts/test_wrapper.sh src/lib_sapling sapling
    - scripts/test_wrapper.sh src/lib_shell shell
    - scripts/test_wrapper.sh src/lib_signer_backends signer_backends
    - scripts/test_wrapper.sh src/lib_signer_backends/unix signer_backends_unix
    - scripts/test_wrapper.sh src/lib_stdlib stdlib
    - scripts/test_wrapper.sh src/lib_stdlib_unix stdlib_unix
    - scripts/test_wrapper.sh src/lib_storage storage
    - scripts/test_wrapper.sh src/proto_006_PsCARTHA/lib_client 006_PsCARTHA_lib_client
    - scripts/test_wrapper.sh src/proto_007_PsDELPH1/lib_client 007_PsDELPH1_lib_client
    - scripts/test_wrapper.sh src/proto_007_PsDELPH1/lib_protocol 007_PsDELPH1_lib_protocol
    - scripts/test_wrapper.sh src/proto_008_PtEdoTez/lib_client 008_PtEdoTez_lib_client
    - scripts/test_wrapper.sh src/proto_008_PtEdoTez/lib_protocol 008_PtEdoTez_lib_protocol
    - scripts/test_wrapper.sh src/proto_alpha/lib_client alpha_lib_client
    - scripts/test_wrapper.sh src/proto_alpha/lib_protocol alpha_lib_protocol
    - scripts/test_wrapper.sh src/tooling src_tooling
    - scripts/test_wrapper.sh vendors/ocaml-bls12-381 ocaml-bls12-381
    - scripts/test_wrapper.sh vendors/ocaml-ledger-wallet ocaml-ledger-wallet
    - scripts/test_wrapper.sh vendors/ocaml-lmdb ocaml-lmdb
    - scripts/test_wrapper.sh vendors/ocaml-uecc ocaml-uecc
  unit:protocol_compiles:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    script:
    - dune build @runtest_compile_protocol
  integration:proto:sandbox:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    before_script:
    - make
    script:
    - dune build @runtest_sandbox
  integration:compiler-rejections:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    before_script:
    - make
    script:
    - dune build @runtest_rejections
  script:prepare_migration_test:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    before_script:
    - last_proto_name=$(find src -name "proto_[0-9][0-9][0-9]_*" | awk -F'/' '{print
      $NF}' | sort -r | head -1)
    - last_proto_version=$(echo $last_proto_name | cut -d'_' -f2)
    - new_proto_version=$(printf "%03d" $((last_proto_version + 1)))
    - make
    script:
    - ./scripts/prepare_migration_test.sh manual "next_$new_proto_version" 1
    allow_failure: true
  integration:sandboxes:voting:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    before_script:
    - make
    script:
    - ROOT_PATH=$PWD/flextesa-voting-demo-noops dune build @src/bin_sandbox/runtest_sandbox_voting_demo_noops
    artifacts:
      paths:
      - flextesa-voting-demo-noops
      expire_in: 1 day
      when: on_failure
    allow_failure: true
  integration:sandboxes:acc-baking:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    before_script:
    - make
    script:
    - ROOT_PATH=$PWD/flextesa-acc-sdb dune build @src/bin_sandbox/runtest_sandbox_accusations_simple_double_baking
    artifacts:
      paths:
      - flextesa-acc-sdb
      expire_in: 1 day
      when: on_failure
  integration:sandboxes:acc-endorsement:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    before_script:
    - make
    script:
    - ROOT_PATH=$PWD/flextesa-acc-sde dune build @src/bin_sandbox/runtest_sandbox_accusations_simple_double_endorsing
    artifacts:
      paths:
      - flextesa-acc-sde
      expire_in: 1 day
      when: on_failure
  integration:sandboxes:u-a-u:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    before_script:
    - make
    script:
    - ROOT_PATH=$PWD/flextesa-hard-fork dune build @src/bin_sandbox/runtest_sandbox_user_activated_upgrade_active
    - ROOT_PATH=$PWD/flextesa-hard-fork dune build @src/bin_sandbox/runtest_sandbox_user_activated_upgrade_alpha
    artifacts:
      paths:
      - flextesa-hard-fork
      expire_in: 1 day
      when: on_failure
  integration:sandboxes:daemons-upgrade:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    before_script:
    - make
    script:
    - ROOT_PATH=$PWD/flextesa-daemons-upgrade dune build @src/bin_sandbox/runtest_sandbox_daemons_upgrade_active
    - ROOT_PATH=$PWD/flextesa-daemons-upgrade dune build @src/bin_sandbox/runtest_sandbox_daemons_upgrade_alpha
    artifacts:
      paths:
      - flextesa-daemons-upgrade
      expire_in: 1 day
      when: on_failure
  integration:007_fast:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007 -k "not slow" -s --log-dir=tmp
    stage: test
  integration:007_baker_endorser:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_baker_endorser.py -s --log-dir=tmp
    stage: test
  integration:007_bootstrap:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_bootstrap.py -s --log-dir=tmp
    stage: test
  integration:007_contract:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_contract.py -s --log-dir=tmp
    stage: test
  integration:007_contract_annotations:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_contract_annotations.py -s --log-dir=tmp
    stage: test
  integration:007_contract_macros:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_contract_macros.py -s --log-dir=tmp
    stage: test
  integration:007_contract_onchain_opcodes:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_contract_onchain_opcodes.py -s --log-dir=tmp
    stage: test
  integration:007_contract_opcodes:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_contract_opcodes.py -s --log-dir=tmp
    stage: test
  integration:007_forge_block:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_forge_block.py -s --log-dir=tmp
    stage: test
  integration:007_many_bakers:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_many_bakers.py -s --log-dir=tmp
    stage: test
  integration:007_many_nodes:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_many_nodes.py -s --log-dir=tmp
    stage: test
  integration:007_mempool:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_mempool.py -s --log-dir=tmp
    stage: test
  integration:007_multinode_snapshot:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_multinode_snapshot.py -s --log-dir=tmp
    stage: test
  integration:007_multinode_storage_reconstruction:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_multinode_storage_reconstruction.py -s --log-dir=tmp
    stage: test
  integration:007_proxy:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_proxy.py -s --log-dir=tmp
    stage: test
  integration:007_rpc:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_007/test_rpc.py -s --log-dir=tmp
    stage: test
  integration:008_fast:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008 -k "not slow" -s --log-dir=tmp
    stage: test
  integration:008_baker_endorser:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_baker_endorser.py -s --log-dir=tmp
    stage: test
  integration:008_bootstrap:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_bootstrap.py -s --log-dir=tmp
    stage: test
  integration:008_contract:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_contract.py -s --log-dir=tmp
    stage: test
  integration:008_contract_annotations:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_contract_annotations.py -s --log-dir=tmp
    stage: test
  integration:008_contract_macros:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_contract_macros.py -s --log-dir=tmp
    stage: test
  integration:008_contract_onchain_opcodes:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_contract_onchain_opcodes.py -s --log-dir=tmp
    stage: test
  integration:008_contract_opcodes:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_contract_opcodes.py -s --log-dir=tmp
    stage: test
  integration:008_forge_block:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_forge_block.py -s --log-dir=tmp
    stage: test
  integration:008_many_bakers:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_many_bakers.py -s --log-dir=tmp
    stage: test
  integration:008_many_nodes:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_many_nodes.py -s --log-dir=tmp
    stage: test
  integration:008_mempool:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_mempool.py -s --log-dir=tmp
    stage: test
  integration:008_multinode_snapshot:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_multinode_snapshot.py -s --log-dir=tmp
    stage: test
  integration:008_multinode_storage_reconstruction:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_multinode_storage_reconstruction.py -s --log-dir=tmp
    stage: test
  integration:008_rpc:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_rpc.py -s --log-dir=tmp
    stage: test
  integration:008_voting_full:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_008/test_voting_full.py -s --log-dir=tmp
    stage: test
  integration:alpha_fast:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha -k "not slow" -s --log-dir=tmp
    stage: test
  integration:alpha_baker_endorser:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_baker_endorser.py -s --log-dir=tmp
    stage: test
  integration:alpha_bootstrap:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_bootstrap.py -s --log-dir=tmp
    stage: test
  integration:alpha_contract:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_contract.py -s --log-dir=tmp
    stage: test
  integration:alpha_contract_annotations:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_contract_annotations.py -s --log-dir=tmp
    stage: test
  integration:alpha_contract_macros:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_contract_macros.py -s --log-dir=tmp
    stage: test
  integration:alpha_contract_onchain_opcodes:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_contract_onchain_opcodes.py -s --log-dir=tmp
    stage: test
  integration:alpha_contract_opcodes:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_contract_opcodes.py -s --log-dir=tmp
    stage: test
  integration:alpha_forge_block:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_forge_block.py -s --log-dir=tmp
    stage: test
  integration:alpha_many_bakers:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_many_bakers.py -s --log-dir=tmp
    stage: test
  integration:alpha_many_nodes:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_many_nodes.py -s --log-dir=tmp
    stage: test
  integration:alpha_mempool:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_mempool.py -s --log-dir=tmp
    stage: test
  integration:alpha_multinode_snapshot:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_multinode_snapshot.py -s --log-dir=tmp
    stage: test
  integration:alpha_multinode_storage_reconstruction:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_multinode_storage_reconstruction.py -s --log-dir=tmp
    stage: test
  integration:alpha_proxy:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_proxy.py -s --log-dir=tmp
    stage: test
  integration:alpha_rpc:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_rpc.py -s --log-dir=tmp
    stage: test
  integration:alpha_voting_full:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - poetry run pytest tests_alpha/test_voting_full.py -s --log-dir=tmp
    stage: test
  integration:examples:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    dependencies:
    - build
    before_script:
    - . $HOME/.venv/bin/activate
    - mkdir tests_python/tmp
    - cd tests_python
    after_script:
    - tail -n +1 tmp/*
    cache: {}
    script:
    - PYTHONPATH=$PYTHONPATH:./ poetry run python examples/forge_transfer.py
    - PYTHONPATH=$PYTHONPATH:./ poetry run python examples/example.py
    - PYTHONPATH=./ poetry run pytest examples/test_example.py
    stage: test
  coq:lint:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    script:
    - make
    - cd src/proto_alpha/lib_protocol
    - for source in *.{ml,mli} ; do coq-of-ocaml -config coq-of-ocaml/config.json $source
      ; done
    allow_failure: true
    retry: 0
  tezt:main:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    before_script:
    - make
    script:
    - dune exec tezt/tests/main.exe -- --color --log-buffer-size 5000 --log-file tezt.log
      --global-timeout 3300 --time
    artifacts:
      paths:
      - tezt.log
      expire_in: 1 day
      when: on_failure
  tezt:manual:migration:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - . ./scripts/version.sh
    - . $HOME/.venv/bin/activate
    interruptible: true
    stage: test
    retry: 2
    cache:
      key: $CI_COMMIT_REF_SLUG
      policy: pull
    when: manual
    before_script:
    - export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y
    - curl -s https://api.github.com/repos/Phlogi/tezos-snapshots/releases/latest |
      jq -r ".assets[] | select(.name) | .browser_download_url" | grep roll | xargs
      wget -q
    - block_hash=$(echo mainnet.roll.* | sed -r 's/mainnet\.roll\.[0-9_-]+\.(.*)\.[0-9]+\.chain\.xz/\1/g')
    - cat mainnet.roll.* | xz -d -v -T0 > mainnet.rolling
    - make
    - scripts/prepare_migration_test.sh auto mainnet.rolling "$block_hash"
    script:
    - dune exec ./tezt/manual_tests/main.exe -- migration --color --log-buffer-size
      5000 --log-file tezt-migration.log
    artifacts:
      when: always
      paths:
      - tezt-migration.log
      expire_in: 30 days
  documentation:build:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    interruptible: true
    stage: build
    except:
    - master
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    script:
    - make -C docs html redirects
    artifacts:
      paths:
      - docs
      expire_in: 1 week
  documentation:linkcheck:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: doc
    before_script:
    - . ./scripts/version.sh
    dependencies:
    - documentation:build
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /doc/
      when: on_success
    - changes:
      - /docs
      when: on_success
    script:
    - make -C docs redirectcheck linkcheck
    allow_failure: true
    interruptible: true
  opam:benchmark-utils:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: benchmark-utils
  opam:bls12-381:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: bls12-381
  opam:flextesa:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: flextesa
  opam:latex:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: latex
  opam:ledgerwallet:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: ledgerwallet
  opam:ledgerwallet-tezos:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: ledgerwallet-tezos
  opam:numerics:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: numerics
  opam:pyml-plot:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: pyml-plot
  opam:staTz:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: staTz
  opam:tezos-007-PsDELPH1-test-helpers:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-007-PsDELPH1-test-helpers
  opam:tezos-008-PtEdoTez-test-helpers:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-008-PtEdoTez-test-helpers
  opam:tezos-accuser-007-PsDELPH1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-accuser-007-PsDELPH1
  opam:tezos-accuser-007-PsDELPH1-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-accuser-007-PsDELPH1-commands
  opam:tezos-accuser-008-PtEdoTez:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-accuser-008-PtEdoTez
  opam:tezos-accuser-008-PtEdoTez-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-accuser-008-PtEdoTez-commands
  opam:tezos-accuser-alpha:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-accuser-alpha
  opam:tezos-accuser-alpha-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-accuser-alpha-commands
  opam:tezos-alpha-test-helpers:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-alpha-test-helpers
  opam:tezos-baker-007-PsDELPH1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-baker-007-PsDELPH1
  opam:tezos-baker-008-PtEdoTez:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-baker-008-PtEdoTez
  opam:tezos-baker-alpha:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-baker-alpha
  opam:tezos-baking-007-PsDELPH1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-baking-007-PsDELPH1
  opam:tezos-baking-007-PsDELPH1-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-baking-007-PsDELPH1-commands
  opam:tezos-baking-008-PtEdoTez:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-baking-008-PtEdoTez
  opam:tezos-baking-008-PtEdoTez-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-baking-008-PtEdoTez-commands
  opam:tezos-baking-alpha:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-baking-alpha
  opam:tezos-baking-alpha-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-baking-alpha-commands
  opam:tezos-base:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-base
  opam:tezos-benchmark:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-benchmark
  opam:tezos-benchmark-examples:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-benchmark-examples
  opam:tezos-benchmark-tests:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-benchmark-tests
  opam:tezos-clic:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-clic
  opam:tezos-client:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client
  opam:tezos-client-000-Ps9mPmXa:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-000-Ps9mPmXa
  opam:tezos-client-001-PtCJ7pwo:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-001-PtCJ7pwo
  opam:tezos-client-001-PtCJ7pwo-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-001-PtCJ7pwo-commands
  opam:tezos-client-002-PsYLVpVv:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-002-PsYLVpVv
  opam:tezos-client-002-PsYLVpVv-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-002-PsYLVpVv-commands
  opam:tezos-client-003-PsddFKi3:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-003-PsddFKi3
  opam:tezos-client-003-PsddFKi3-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-003-PsddFKi3-commands
  opam:tezos-client-004-Pt24m4xi:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-004-Pt24m4xi
  opam:tezos-client-004-Pt24m4xi-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-004-Pt24m4xi-commands
  opam:tezos-client-005-PsBabyM1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-005-PsBabyM1
  opam:tezos-client-005-PsBabyM1-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-005-PsBabyM1-commands
  opam:tezos-client-006-PsCARTHA:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-006-PsCARTHA
  opam:tezos-client-006-PsCARTHA-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-006-PsCARTHA-commands
  opam:tezos-client-007-PsDELPH1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-007-PsDELPH1
  opam:tezos-client-007-PsDELPH1-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-007-PsDELPH1-commands
  opam:tezos-client-007-PsDELPH1-commands-registration:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-007-PsDELPH1-commands-registration
  opam:tezos-client-008-PtEdoTez:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-008-PtEdoTez
  opam:tezos-client-008-PtEdoTez-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-008-PtEdoTez-commands
  opam:tezos-client-008-PtEdoTez-commands-registration:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-008-PtEdoTez-commands-registration
  opam:tezos-client-alpha:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-alpha
  opam:tezos-client-alpha-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-alpha-commands
  opam:tezos-client-alpha-commands-registration:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-alpha-commands-registration
  opam:tezos-client-base:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-base
  opam:tezos-client-base-unix:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-base-unix
  opam:tezos-client-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-commands
  opam:tezos-client-demo-counter:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-demo-counter
  opam:tezos-client-genesis:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-genesis
  opam:tezos-client-genesis-carthagenet:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-genesis-carthagenet
  opam:tezos-client-sapling-008-PtEdoTez:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-sapling-008-PtEdoTez
  opam:tezos-client-sapling-alpha:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-client-sapling-alpha
  opam:tezos-codec:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-codec
  opam:tezos-crypto:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-crypto
  opam:tezos-embedded-protocol-000-Ps9mPmXa:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-000-Ps9mPmXa
  opam:tezos-embedded-protocol-001-PtCJ7pwo:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-001-PtCJ7pwo
  opam:tezos-embedded-protocol-002-PsYLVpVv:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-002-PsYLVpVv
  opam:tezos-embedded-protocol-003-PsddFKi3:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-003-PsddFKi3
  opam:tezos-embedded-protocol-004-Pt24m4xi:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-004-Pt24m4xi
  opam:tezos-embedded-protocol-005-PsBABY5H:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-005-PsBABY5H
  opam:tezos-embedded-protocol-005-PsBabyM1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-005-PsBabyM1
  opam:tezos-embedded-protocol-006-PsCARTHA:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-006-PsCARTHA
  opam:tezos-embedded-protocol-007-PsDELPH1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-007-PsDELPH1
  opam:tezos-embedded-protocol-008-PtEdoTez:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-008-PtEdoTez
  opam:tezos-embedded-protocol-alpha:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-alpha
  opam:tezos-embedded-protocol-demo-counter:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-demo-counter
  opam:tezos-embedded-protocol-demo-noops:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-demo-noops
  opam:tezos-embedded-protocol-genesis:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-genesis
  opam:tezos-embedded-protocol-genesis-carthagenet:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-embedded-protocol-genesis-carthagenet
  opam:tezos-endorser-007-PsDELPH1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-endorser-007-PsDELPH1
  opam:tezos-endorser-007-PsDELPH1-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-endorser-007-PsDELPH1-commands
  opam:tezos-endorser-008-PtEdoTez:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-endorser-008-PtEdoTez
  opam:tezos-endorser-008-PtEdoTez-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-endorser-008-PtEdoTez-commands
  opam:tezos-endorser-alpha:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-endorser-alpha
  opam:tezos-endorser-alpha-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-endorser-alpha-commands
  opam:tezos-error-monad:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-error-monad
  opam:tezos-event-logging:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-event-logging
  opam:tezos-lmdb:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-lmdb
  opam:tezos-lwt-result-stdlib:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-lwt-result-stdlib
  opam:tezos-mempool-007-PsDELPH1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-mempool-007-PsDELPH1
  opam:tezos-mempool-008-PtEdoTez:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-mempool-008-PtEdoTez
  opam:tezos-mempool-alpha:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-mempool-alpha
  opam:tezos-micheline:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-micheline
  opam:tezos-micheline-rewriting:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-micheline-rewriting
  opam:tezos-mockup:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-mockup
  opam:tezos-mockup-commands:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-mockup-commands
  opam:tezos-mockup-proxy:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-mockup-proxy
  opam:tezos-mockup-registration:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-mockup-registration
  opam:tezos-node:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-node
  opam:tezos-p2p:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-p2p
  opam:tezos-p2p-services:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-p2p-services
  opam:tezos-protocol-000-Ps9mPmXa:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-000-Ps9mPmXa
  opam:tezos-protocol-001-PtCJ7pwo:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-001-PtCJ7pwo
  opam:tezos-protocol-002-PsYLVpVv:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-002-PsYLVpVv
  opam:tezos-protocol-003-PsddFKi3:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-003-PsddFKi3
  opam:tezos-protocol-004-Pt24m4xi:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-004-Pt24m4xi
  opam:tezos-protocol-005-PsBABY5H:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-005-PsBABY5H
  opam:tezos-protocol-005-PsBabyM1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-005-PsBabyM1
  opam:tezos-protocol-006-PsCARTHA:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-006-PsCARTHA
  opam:tezos-protocol-006-PsCARTHA-parameters:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-006-PsCARTHA-parameters
  opam:tezos-protocol-007-PsDELPH1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-007-PsDELPH1
  opam:tezos-protocol-007-PsDELPH1-parameters:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-007-PsDELPH1-parameters
  opam:tezos-protocol-007-PsDELPH1-tests:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-007-PsDELPH1-tests
  opam:tezos-protocol-008-PtEdoTez:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-008-PtEdoTez
  opam:tezos-protocol-008-PtEdoTez-parameters:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-008-PtEdoTez-parameters
  opam:tezos-protocol-008-PtEdoTez-tests:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-008-PtEdoTez-tests
  opam:tezos-protocol-alpha:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-alpha
  opam:tezos-protocol-alpha-parameters:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-alpha-parameters
  opam:tezos-protocol-alpha-tests:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-alpha-tests
  opam:tezos-protocol-compiler:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-compiler
  opam:tezos-protocol-demo-counter:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-demo-counter
  opam:tezos-protocol-demo-noops:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-demo-noops
  opam:tezos-protocol-environment:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-environment
  opam:tezos-protocol-environment-packer:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-environment-packer
  opam:tezos-protocol-environment-sigs:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-environment-sigs
  opam:tezos-protocol-environment-structs:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-environment-structs
  opam:tezos-protocol-functor-000-Ps9mPmXa:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-000-Ps9mPmXa
  opam:tezos-protocol-functor-001-PtCJ7pwo:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-001-PtCJ7pwo
  opam:tezos-protocol-functor-002-PsYLVpVv:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-002-PsYLVpVv
  opam:tezos-protocol-functor-003-PsddFKi3:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-003-PsddFKi3
  opam:tezos-protocol-functor-004-Pt24m4xi:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-004-Pt24m4xi
  opam:tezos-protocol-functor-005-PsBABY5H:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-005-PsBABY5H
  opam:tezos-protocol-functor-005-PsBabyM1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-005-PsBabyM1
  opam:tezos-protocol-functor-006-PsCARTHA:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-006-PsCARTHA
  opam:tezos-protocol-functor-007-PsDELPH1:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-007-PsDELPH1
  opam:tezos-protocol-functor-008-PtEdoTez:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-008-PtEdoTez
  opam:tezos-protocol-functor-alpha:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-alpha
  opam:tezos-protocol-functor-demo-counter:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-demo-counter
  opam:tezos-protocol-functor-demo-noops:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-demo-noops
  opam:tezos-protocol-functor-genesis:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-genesis
  opam:tezos-protocol-functor-genesis-carthagenet:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-functor-genesis-carthagenet
  opam:tezos-protocol-genesis:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-genesis
  opam:tezos-protocol-genesis-carthagenet:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-genesis-carthagenet
  opam:tezos-protocol-updater:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-protocol-updater
  opam:tezos-proxy:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-proxy
  opam:tezos-requester:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-requester
  opam:tezos-rpc:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-rpc
  opam:tezos-rpc-http:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-rpc-http
  opam:tezos-rpc-http-client:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-rpc-http-client
  opam:tezos-rpc-http-client-unix:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-rpc-http-client-unix
  opam:tezos-rpc-http-server:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-rpc-http-server
  opam:tezos-sapling:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-sapling
  opam:tezos-shell:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-shell
  opam:tezos-shell-benchmarks:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-shell-benchmarks
  opam:tezos-shell-context:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-shell-context
  opam:tezos-shell-services:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-shell-services
  opam:tezos-signer:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-signer
  opam:tezos-signer-backends:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-signer-backends
  opam:tezos-signer-services:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-signer-services
  opam:tezos-snoop:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-snoop
  opam:tezos-stdlib:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-stdlib
  opam:tezos-stdlib-unix:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-stdlib-unix
  opam:tezos-storage:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-storage
  opam:tezos-test-services:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-test-services
  opam:tezos-tooling:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-tooling
  opam:tezos-validation:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-validation
  opam:tezos-validator:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-validator
  opam:tezos-version:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-version
  opam:tezos-workers:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: tezos-workers
  opam:uecc:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: packaging
    dependencies: []
    rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH == "proto-proposal"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /opam/
      when: on_success
    - changes:
      - /**/*.opam
      - /**/dune
      - /**/dune.inc
      - /**/*.dune.inc
      - /scripts/version.sh
      - /.gitlab-ci.yml
      when: on_success
    script:
    - ./scripts/opam-pin.sh
    - opam depext --yes ${package}
    - opam install --yes ${package}
    - opam reinstall --yes --with-test ${package}
    interruptible: true
    variables:
      package: uecc
  build_release:docker_amd64:
    image: docker:latest
    services:
    - docker:dind
    variables:
      DOCKER_DRIVER: overlay2
      IMAGE_ARCH_PREFIX: 
    stage: build_release
    only:
    - master@tezos/tezos
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - mkdir ~/.docker || true
    - echo "{ \"auths\":{ \"https://index.docker.io/v1/\":{ \"auth\":\"${CI_DOCKER_AUTH}\"
      } } }" > ~/.docker/config.json
    script:
    - ./scripts/create_docker_image.sh "${public_docker_image_name}" "${IMAGE_ARCH_PREFIX}${CI_COMMIT_REF_NAME}"
      "${build_deps_image_name}" "${build_deps_image_version}" "${CI_COMMIT_SHORT_SHA}"
    - docker push "${public_docker_image_name}:${IMAGE_ARCH_PREFIX}${CI_COMMIT_REF_NAME}"
    - docker push "${public_docker_image_name}-bare:${IMAGE_ARCH_PREFIX}${CI_COMMIT_REF_NAME}"
    - docker push "${public_docker_image_name}-debug:${IMAGE_ARCH_PREFIX}${CI_COMMIT_REF_NAME}"
    interruptible: false
    variables:
      DOCKER_DRIVER: overlay2
      IMAGE_ARCH_PREFIX: amd64-
    tags:
    - safe_docker
  build_release:docker_arm64:
    image: docker:latest
    services:
    - docker:dind
    variables:
      DOCKER_DRIVER: overlay2
      IMAGE_ARCH_PREFIX: 
    stage: build_release
    only:
    - master@tezos/tezos
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - mkdir ~/.docker || true
    - echo "{ \"auths\":{ \"https://index.docker.io/v1/\":{ \"auth\":\"${CI_DOCKER_AUTH}\"
      } } }" > ~/.docker/config.json
    script:
    - ./scripts/create_docker_image.sh "${public_docker_image_name}" "${IMAGE_ARCH_PREFIX}${CI_COMMIT_REF_NAME}"
      "${build_deps_image_name}" "${build_deps_image_version}" "${CI_COMMIT_SHORT_SHA}"
    - docker push "${public_docker_image_name}:${IMAGE_ARCH_PREFIX}${CI_COMMIT_REF_NAME}"
    - docker push "${public_docker_image_name}-bare:${IMAGE_ARCH_PREFIX}${CI_COMMIT_REF_NAME}"
    - docker push "${public_docker_image_name}-debug:${IMAGE_ARCH_PREFIX}${CI_COMMIT_REF_NAME}"
    interruptible: false
    variables:
      DOCKER_DRIVER: overlay2
      IMAGE_ARCH_PREFIX: arm64-
    tags:
    - arm64
  build_release:static-x86_64-linux-binaries:
    stage: build_release
    image: ${build_deps_image_name}:runtime-build-dependencies--${build_deps_image_version}
    rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /-release$/
      when: on_success
    - if: $CI_COMMIT_TAG != null
      when: on_success
    before_script:
    - sudo apk --no-cache --virtual add unzip wget eudev-dev autoconf automake libtool
      linux-headers binutils
    - packaging/build/build-libusb-and-hidapi.sh
    - git apply packaging/build/static_libs.patch
    - dune build @install --profile release
    - dune install --prefix install_root
    - find . -maxdepth 1 -type f ! -name "*.*" -exec strip --strip-debug {} \;
    artifacts:
      paths:
      - install_root/bin/*
    script:
    - sudo apk --no-cache --virtual add upx
    - find . -maxdepth 1 -type f ! -name "*.*" -exec upx {} \;
  build_release:static-arm64-linux-binaries:
    stage: build_release
    image: ${build_deps_image_name}:runtime-build-dependencies--${build_deps_image_version}
    rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /-release$/
      when: on_success
    - if: $CI_COMMIT_TAG != null
      when: on_success
    before_script:
    - sudo apk --no-cache --virtual add unzip wget eudev-dev autoconf automake libtool
      linux-headers binutils
    - packaging/build/build-libusb-and-hidapi.sh
    - git apply packaging/build/static_libs.patch
    - dune build @install --profile release
    - dune install --prefix install_root
    - find . -maxdepth 1 -type f ! -name "*.*" -exec strip --strip-debug {} \;
    artifacts:
      paths:
      - install_root/bin/*
    rules:
    - if: $CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAMESPACE == "tezos"
      when: on_success
    - if: $CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: on_success
    - if: $CI_COMMIT_TAG != null && $CI_PROJECT_NAMESPACE == "tezos"
      when: on_success
    script:
    - echo "No compression for now"
    tags:
    - arm64
  release-static-x86_64-binaries:
    image: registry.gitlab.com/gitlab-org/release-cli
    variables:
      ARCH_PREFIX: 
    rules:
    - if: $CI_COMMIT_TAG =~ /\A\d+\.\d+\.\d+\z/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: on_success
    variables:
      PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/tezos/${CI_COMMIT_TAG}
    stage: publish_release
    script:
    - apk --no-cache --virtual add bash jq curl
    - scripts/release/upload-static-binaries-to-package-registry.sh "$ARCH_PREFIX"
    variables:
      ARCH_PREFIX: x86_64-
    dependencies:
    - build_release:static-x86_64-linux-binaries
  release-static-arm64-binaries:
    image: registry.gitlab.com/gitlab-org/release-cli
    variables:
      ARCH_PREFIX: 
    rules:
    - if: $CI_COMMIT_TAG =~ /\A\d+\.\d+\.\d+\z/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: on_success
    variables:
      PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/tezos/${CI_COMMIT_TAG}
    stage: publish_release
    script:
    - apk --no-cache --virtual add bash jq curl
    - scripts/release/upload-static-binaries-to-package-registry.sh "$ARCH_PREFIX"
    variables:
      ARCH_PREFIX: arm64-
    dependencies:
    - build_release:static-arm64-linux-binaries
  release-on-gitlab:
    image: registry.gitlab.com/gitlab-org/release-cli
    rules:
    - if: $CI_COMMIT_TAG =~ /\A\d+\.\d+\.\d+\z/ && $CI_PROJECT_NAMESPACE == "tezos"
      when: on_success
    variables:
      PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/tezos/${CI_COMMIT_TAG}
    stage: publish_release
    script:
    - apk --no-cache --virtual add bash jq
    - scripts/release/create-release-with-static-binaries.sh
  merge-manifest:
    image: docker:latest
    services:
    - name: docker:dind
      command:
      - --experimental
    variables:
      DOCKER_DRIVER: overlay2
    stage: publish_release
    only:
    - master@tezos/tezos
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    before_script:
    - apk add git binutils
    - mkdir ~/.docker || true
    - echo "{ \"experimental\":\"enabled\", \"auths\":{ \"https://index.docker.io/v1/\":{
      \"auth\":\"${CI_DOCKER_AUTH}\" } } }" > ~/.docker/config.json
    script:
    - LAST_COMMIT_DATE_TIME=$(git log --pretty=format:"%cd" -1 --date="format:%Y%m%d%H%M%S"
      2>&1)
    - docker manifest create "${public_docker_image_name}-bare:${CI_COMMIT_REF_NAME}"
      --amend "${public_docker_image_name}-bare:amd64-${CI_COMMIT_REF_NAME}" --amend
      "${public_docker_image_name}-bare:arm64-${CI_COMMIT_REF_NAME}"
    - docker manifest push "${public_docker_image_name}-bare:${CI_COMMIT_REF_NAME}"
    - docker manifest create "${public_docker_image_name}-bare:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHORT_SHA}_${LAST_COMMIT_DATE_TIME}"
      --amend "${public_docker_image_name}-bare:amd64-${CI_COMMIT_REF_NAME}" --amend
      "${public_docker_image_name}-bare:arm64-${CI_COMMIT_REF_NAME}"
    - docker manifest push "${public_docker_image_name}-bare:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHORT_SHA}_${LAST_COMMIT_DATE_TIME}"
    - docker manifest create "${public_docker_image_name}-debug:${CI_COMMIT_REF_NAME}"
      --amend "${public_docker_image_name}-debug:amd64-${CI_COMMIT_REF_NAME}" --amend
      "${public_docker_image_name}-debug:arm64-${CI_COMMIT_REF_NAME}"
    - docker manifest push "${public_docker_image_name}-debug:${CI_COMMIT_REF_NAME}"
    - docker manifest create "${public_docker_image_name}-debug:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHORT_SHA}_${LAST_COMMIT_DATE_TIME}"
      --amend "${public_docker_image_name}-debug:amd64-${CI_COMMIT_REF_NAME}" --amend
      "${public_docker_image_name}-debug:arm64-${CI_COMMIT_REF_NAME}"
    - docker manifest push "${public_docker_image_name}-debug:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHORT_SHA}_${LAST_COMMIT_DATE_TIME}"
    - docker manifest create "${public_docker_image_name}:${CI_COMMIT_REF_NAME}" --amend
      "${public_docker_image_name}:amd64-${CI_COMMIT_REF_NAME}" --amend "${public_docker_image_name}:arm64-${CI_COMMIT_REF_NAME}"
    - docker manifest push "${public_docker_image_name}:${CI_COMMIT_REF_NAME}"
    - docker manifest create "${public_docker_image_name}:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHORT_SHA}_${LAST_COMMIT_DATE_TIME}"
      --amend "${public_docker_image_name}:amd64-${CI_COMMIT_REF_NAME}" --amend "${public_docker_image_name}:arm64-${CI_COMMIT_REF_NAME}"
    - docker manifest push "${public_docker_image_name}:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHORT_SHA}_${LAST_COMMIT_DATE_TIME}"
    interruptible: false
  publish:doc:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: doc
    only:
    - master@tezos/tezos
    before_script:
    - sudo apk add --no-cache openssh-client rsync
    - echo "${CI_PK_GITLAB_DOC}" > ~/.ssh/id_ed25519
    - echo "${CI_KH}" > ~/.ssh/known_hosts
    - chmod 400 ~/.ssh/id_ed25519
    script:
    - make -C docs all
    - git clone --depth 5 git@gitlab.com:${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAMESPACE}.gitlab.io
      gitlab.io
    - rsync --recursive --links --perms --delete --verbose --exclude=.doctrees --exclude={{main,alpha,zero}net,master}/index.html
      docs/_build/ gitlab.io/public/
    - cd gitlab.io
    - if [ -z "$(git status -s)" ] ; then echo "Nothing to commit!" ; else git add public
      ; git commit -m "Import doc of ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_SHA}"
      ; git push origin master ; fi
    interruptible: false
  pages:
    when: manual
    cache:
      paths:
      - public
    stage: publish_coverage
    except:
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    dependencies:
    - test_coverage
    script:
    - mkdir -p public
    - mv _coverage_report public/$CI_PIPELINE_ID
    - echo "Coverage report published at $CI_PAGES_URL/$CI_PIPELINE_ID"
    artifacts:
      paths:
      - public
      expire_in: 7 days
    interruptible: false
  test_coverage:
    image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}
    stage: test_coverage
    except:
    - tags@tezos/tezos
    - /-release$/@tezos/tezos
    when: manual
    variables:
      OLD_PROTOCOLS: src/proto_000_Ps9mPmXa src/proto_001_PtCJ7pwo src/proto_002_PsYLVpVv
        src/proto_003_PsddFKi3 src/proto_004_Pt24m4xi src/proto_005_PsBABY5H src/proto_005_PsBabyM1
        src/proto_006_PsCARTHA
      NOT_INSTRUMENTABLE: src/proto_007_PsDELPH1 src/proto_alpha
      COVERAGE_EXCLUDE: $OLD_PROTOCOLS $NOT_INSTRUMENTABLE
    script:
    - scripts/instrument_dune_bisect.sh src/ --except $COVERAGE_EXCLUDE
    - make
    - . $HOME/.venv/bin/activate
    - make test-coverage || true
    - make coverage-report
    - make coverage-report-summary
    - touch $CI_PROJECT_DIR/__success
    after_script:
    - ! "if [ ! -f __success ]; then\n  echo \"Job was unable to generate the coverage
      report.\"\n  echo \"Check http://tezos.gitlab.io/developer/testing.html#measuring-test-coverage\"\n
      \ echo \"for a list of known issues.\"\nfi\n"
    coverage: ! '/Coverage: \d+\/\d+ \(([^%]+%)\)/'
    artifacts:
      when: always
      paths:
      - _coverage_report/
      expire_in: 15 days
