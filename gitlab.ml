(* Suppose extensions are prefixed with a dot *)
let filter_extend_definitions yaml_objects =
  List.partition (fun (key, _) -> '.' = key.[0]) yaml_objects

(** Finds and returns all includes, merged *)
let get_other_yaml_paths base_dir (yaml_object : (string * Yaml.value) list) :
    (string * Yaml.value) list =
  match (List.find_opt (fun (key, _) -> "include" = key) yaml_object) with
  | Some (_, include_values) ->
     let l =
       match include_values with
       | `A include_list ->
          List.map
            (fun v ->
              match v with
              | `String path -> (
                let yaml = Yaml_unix.of_file_exn Fpath.(base_dir // v path) in
                match yaml with
                | `O yaml_object ->
                   yaml_object
                | _ ->
                   failwith "Expected YAML object" )
              | _ ->
                 failwith "String expected in include")
            include_list
       | _ ->
          failwith "Unexpected type for the include key"
     in
     List.concat l
  | None ->
     []

(** Optionally returns the template this job extends *)
let get_extend_name job_desc =
  match List.find_opt (fun (k, _v) -> k = "extends") job_desc with
  | None ->
      None
  | Some (_k, v) -> (
    match v with `String s -> Some s | _ -> failwith "Expected string value" )

(* replace extends section of the job. Not using a hashmap because we need
   to keep the order. We should stop directly when we found the key "extends" *)
let of_object ?err v =
  match v with
  | `O l ->
      l
  | _ ->
      failwith
      @@ Option.value err ~default:"Expected object"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Double_quoted v

(** Replaces the [extends: v] key-value pair, if present, in an obj with the key-
    values in [extend_content] *)
let replace_extend_by_definition job_desc extend_content =
  (* TODO: should handle lists here as well *)
  let extend_content =
    of_object
      ~err:"Only extend contents with object is supported"
      extend_content
  in
  List.rev
    (List.fold_left
       (fun acc (k, v) ->
         if k = "extends" then List.concat [List.rev extend_content; acc]
         else (k, v) :: acc)
       []
       job_desc)

(** Replaces the [extends: v] key-value pair, if present, in the obj [job] using the
    values in  [extend_definitions]. solve_extend is called recursively until all extends
    have been resolved. *)
let rec solve_extend extend_definitions job =
  let (job_name, job_desc) = job in
  let job_desc =
    of_object ~err:"Only job as objects are supported for the moment" job_desc
  in
  let extend_name_opt = get_extend_name job_desc in
  match extend_name_opt with
  | None ->
      job
  | Some extend_name ->
      let extend_content =
        snd (List.find (fun (k, _) -> k = extend_name) extend_definitions)
      in
      let expanded_job_desc =
        replace_extend_by_definition job_desc extend_content
      in
      (* Recursive call in case of nested extends *)
      solve_extend extend_definitions (job_name, `O expanded_job_desc)

let special_sections = ["stages"; "variables"; "include"]

let solve_extends extend_defintions jobs =
  List.map
    (fun job ->
      if List.mem (fst job) special_sections then job
      else solve_extend extend_defintions job)
    jobs

let remove_include_section yaml_objects =
  let section_to_remove = ["include"] in
  List.filter (fun (k, _) -> not (List.mem k section_to_remove)) yaml_objects

let filter_jobs yaml_objects =
  let section_to_ignore = special_sections in
  List.partition (fun (k, _) -> List.mem k section_to_ignore) yaml_objects

let merge base_dir yaml_object =
  let all_includes = get_other_yaml_paths base_dir yaml_object in
  let yaml_objects = List.concat [yaml_object; all_includes] in
  (* print_endline (Yaml.to_string_exn (`O yaml_objects)) ; *)
  let (extends, yaml_objects) = filter_extend_definitions yaml_objects in
  let yaml_objects = remove_include_section yaml_objects in
  let expanded_yaml_objects = solve_extends extends yaml_objects in
  expanded_yaml_objects
