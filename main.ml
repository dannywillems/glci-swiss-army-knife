open Tezos_clic
open Tezos_error_monad
open Error_monad

(* actions:
    - [x] focus
      - [ ] Should complain on non-existing job
    - [x] merge
    - [x] lint
    - [ ] visualize
    - [ ] diff
    - [ ] compare

    - other:
      - should be possible to just specicy a dir and not dir/.gitlab-ci.yml
*)

type gl_context = {base_dir : Fpath.t; ci_config : (string * Yaml.value) list}

let default_gl_context : gl_context = {ci_config = []; base_dir = Fpath.v "."}

let group = {Clic.name = "gitlab"; title = "Gitlab CI file commands"}

module GlClic = struct
  let path_parameter =
    Clic.parameter (fun (_ctxt : gl_context) p ->
        return p
        (* if Sys.file_exists p then return p *)
        (* else failwith "Not an existing file: '%s'" p *)
      )
end

module GlYaml = struct
  let to_string_exn obj = Yaml.to_string_exn
                 (* ~scalar_style:`Double_quoted *)
                 obj

  let to_string_err v =
    match Yaml.to_string v with
    | Ok s -> return s
    | Error (`Msg msg) -> failwith "%s" msg

  let roundabout_to_string_err obj =
    (match obj with
     | `O kvs ->
        (* this works *)
        Tezos_lwt_result_stdlib.Lwtreslib.List.map_ep
          (fun (k, v) -> (to_string_err (`O [(k,v)])))
          kvs >|=? String.concat ""
     (* this doesnt: ?? *)
     (* Yaml.pp (`O obj) ; *)
     | v ->
        to_string_err v)

  let pp_roundabout fmt obj : unit =
    (match obj with
     | `O kvs ->
        (* this works *)
        List.iter (fun (k, v) -> Format.fprintf fmt "%a" Yaml.pp (`O [(k, v)])) kvs
          (* this doesnt: ?? *)
          (* Yaml.pp (`O obj) ; *)
     | v ->
        Yaml.pp fmt v)

  let to_string_roundabout obj =
    Format.asprintf "%a" pp_roundabout obj

  (* let yaml_val_to_json (y : Yaml.value) : Yojson.Basic.t =
   *   let rec f : Yaml.value -> Yojson.Basic.t = function
   *     | `Bool b ->
   *        `Bool b
   *     | `Float f ->
   *        `Float f
   *     | `String s ->
   *        `String s
   *     | `Null ->
   *        `Null
   *     | `A l ->
   *        `List (List.map f l)
   *     | `O kvs ->
   *        `Assoc (List.map (fun (k, v) -> (k, f v)) kvs)
   *   in
   *   f y *)

end

module Global_options = struct
  let ci_file =
    Clic.default_arg
      ~doc:"The .gitlab-ci.yml file"
      ~long:"ci-file"
      ~placeholder:".gitlab-ci.yml"
      ~default:".gitlab-ci.yml"
      GlClic.path_parameter

  let options = Clic.args1 ci_file
end

let output obj = Format.printf "%a" GlYaml.pp_roundabout obj

(* let file_printer ?(file = ".gitlab-ci.yml") obj =
 *   Bos.OS.File.write Fpath.(v file) (GlYaml.pp obj) *)

module Action_focus = struct
  let options = Clic.no_options

  let params =
    Clic.(
      prefix "focus" @@ string ~name:"job" ~desc:"Job we want to focus" @@ stop)

  let focus :
      unit ->
      string ->
      gl_context ->
      unit Tezos_error_monad.Error_monad.tzresult Lwt.t =
   fun () focused_job {ci_config; base_dir} ->
    let expanded_yaml_objects = Gitlab.merge base_dir ci_config in
    let (ignored_sections, jobs) = Gitlab.filter_jobs expanded_yaml_objects in
    let filtered_jobs =
      List.filter (fun (job_name, _job_desc) -> job_name = focused_job) jobs
    in
    let final_content = List.concat [ignored_sections; filtered_jobs] in
    output (`O final_content) ;
    return_unit

  let command = Clic.command ~desc:"Focus a job" ~group options params focus
end

module Action_merge = struct
  let options = Clic.no_options

  let params = Clic.(prefix "merge" @@ stop)

  let merge :
      unit -> gl_context -> unit Tezos_error_monad.Error_monad.tzresult Lwt.t =
   fun () {ci_config; base_dir} ->
    let expanded_yaml_objects = Gitlab.merge base_dir ci_config in
    output (`O expanded_yaml_objects) ;
    return_unit

  let command = Clic.command ~desc:"Merge CI file" ~group options params merge
end

module Action_lint = struct
  let options = Clic.no_options

  let params = Clic.(prefix "lint" @@ stop)

  let merge :
      unit -> gl_context -> unit Tezos_error_monad.Error_monad.tzresult Lwt.t =
   fun () {ci_config; base_dir} ->
    let ci_config = Gitlab.merge base_dir ci_config in
    GlYaml.roundabout_to_string_err (`O ci_config)
    >>=? Gitlab_api.lint
    >>=? fun res ->
    Format.printf "%a\n" Yojson.Basic.pp res ;
    return_unit

  let command = Clic.command ~desc:"Merge CI file" ~group options params merge
end

let commands = [Action_focus.command; Action_merge.command; Action_lint.command]

let commands_with_man =
  Clic.add_manual
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options:Global_options.options
    (if Unix.isatty Unix.stdout then Clic.Ansi else Clic.Plain)
    Format.std_formatter
    commands

let setup_context ci_config_path =
  (if Sys.file_exists ci_config_path
   then return ()
   else (failwith "Not an existing file: '%s'" ci_config_path))
  >>=? fun () ->
  let ci_config_path = Fpath.v ci_config_path in
  let base_dir = fst (Fpath.split_base ci_config_path) in
  let ci_config = Yaml_unix.of_file_exn ci_config_path in
  match ci_config with
  | `O ci_config ->
     return {ci_config; base_dir}
  | _ ->
     failwith "Invalid GitLab CI file %a" Fpath.pp ci_config_path

let () =
  ignore
    Clic.(
      setup_formatter
        Format.std_formatter
        (if Unix.isatty Unix.stdout then Ansi else Plain)
        Short) ;
  let original_args = Array.to_list Sys.argv |> List.tl in
  let result =
    Lwt_main.run
      ( Clic.parse_global_options
          Global_options.options
          default_gl_context
          original_args
      >>=? fun (global_args, args) ->
      setup_context global_args
      >>=? fun ctx -> Clic.dispatch commands_with_man ctx args )
  in
  match result with
  | Ok global_options ->
      global_options
  | Error [Clic.Help command] ->
      Clic.usage
        Format.std_formatter
        ~executable_name:(Filename.basename Sys.executable_name)
        ~global_options:Global_options.options
        (match command with None -> [] | Some c -> [c]) ;
      exit 0
  | Error errors ->
      Clic.pp_cli_errors
        Format.err_formatter
        ~executable_name:(Filename.basename Sys.executable_name)
        ~global_options:Clic.no_options
        ~default:(fun fmt err -> Error_monad.pp_print_error fmt [err])
        errors ;
      exit 1

let _ =
  Gitlab_api.json_body
    "http://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1"
