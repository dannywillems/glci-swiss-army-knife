# Generate GitLab CI file for local usage

Since commit 3f48ed9a97df96f587fbfb99334fdda63a69c971 (and others just before),
`.gitlab-ci.yml` has been splitted in different YAML files, and use `include` in
addition to `extends` to define jobs.
However, these methods are not supported by `gitlab-runner` when attempting to
run it locally (see [this
issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27205) for
instance).
Until a fix is provided in `gitlab-runner`, here a small script to generate a
`.gitlab-ci.yml` file for a single job (generating the file for all jobs fail
with an error). It will replace the file in the Tezos home directory and you can
use the usual `gitlab-runner exec docker [job_name]`

If required, this script could be improved to support more usecases later.

## Installation

Requires [ocaml-yaml](https://github.com/avsm/ocaml-yaml) (tested with 2.1.0)
```shell
opam install yaml
```

## Usage

```shell
# From Tezos home directory
dune exec scripts/generate-local-gitlab-ci/generate_local_gitlab_ci.exe sanity
```
