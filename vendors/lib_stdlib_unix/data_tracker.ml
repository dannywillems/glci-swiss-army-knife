(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
open Data_tracker_store

let debug = ref false

module type Tracker = sig
  type t

  val create : ?register:unit -> ?section:string -> ?name:string -> unit -> t

  val track : t -> 'a -> unit

  val tracked : t -> int

  val pp_distrib :
    ?buckets_count:int -> name_size:int -> Format.formatter -> t -> unit

  val register : section:string -> t -> unit

  val unregister : t -> unit

  val pp_registered :
    ?buckets_count:int ->
    ?sort:unit ->
    unit ->
    Format.formatter ->
    unit ->
    unit
end

module type Store = sig
  type 'metadata t

  type handle

  type packed_key = Pack_k : 'a -> packed_key [@@inlined]

  val create : ?increment:int -> ?size:int -> unit -> 'metadata t

  val add : 'metadata t -> 'a -> 'metadata -> handle

  val fold : (packed_key -> 'metadata -> 'a -> 'a) -> 'a -> 'metadata t -> 'a

  val size : 'metadata t -> int
end

module type Metadata = sig
  type t

  val start : 'a -> t

  type stats

  val init : unit -> stats

  val accumulate : stats -> t -> stats

  type reference_point

  val current_reference : unit -> reference_point

  val compute_bucket :
    t -> stats -> reference_point -> buckets_count:int -> int

  type live_stats

  val init_live_stats : stats -> live_stats

  val update_stats : live_stats -> t -> live_stats

  val compare : live_stats -> live_stats -> int

  type local_stats

  val init_local_stats : local_stats

  val update_local_stats : local_stats -> t -> local_stats

  val pp_stats : Format.formatter -> live_stats * reference_point -> unit

  val pp_local_stats : Format.formatter -> int * local_stats -> unit
end

module Tracker (Store : Store) (Metadata : Metadata) : Tracker = struct
  type t = {
    mutable stats : Metadata.stats;
    set : Metadata.t Store.t;
    name : string;
  }

  let registered : (string * t list) list ref = ref []

  let assoc_add section data lst =
    let rec aux section data lst res =
      match lst with
      | [] ->
          (section, [data]) :: res
      | (sec, lst) :: tl when sec = section ->
          (sec, data :: lst) :: List.rev_append tl res
      | h :: t ->
          aux section data t (h :: res)
    in
    aux section data lst []

  let register ~section t = registered := assoc_add section t !registered

  let reg = register

  let unregister t =
    registered :=
      List.map
        (fun (section, ts) -> (section, List.filter (fun t' -> t != t') ts))
        !registered

  let create ?register ?(section = "") ?(name = "") () =
    let tracker = {stats = Metadata.init (); set = Store.create (); name} in
    Option.iter (fun () -> reg ~section tracker) register ;
    tracker

  let track t data =
    if !debug then (
      let metadata = Metadata.start data in
      t.stats <- Metadata.accumulate t.stats metadata ;
      ignore (Store.add t.set data metadata) )

  let tracked t = Store.size t.set

  let compute_distrib_internal ?(buckets_count = 5) set ref_point =
    let distrib = Array.make buckets_count (0, Metadata.init_local_stats) in
    let (distrib, live_stats) =
      Store.fold
        (fun _ metadata (distrib, live_stats) ->
          (* for all metadata associated to a live value,
             - select the relevant bucket *)
          let bucket_number =
            Metadata.compute_bucket metadata set.stats ref_point ~buckets_count
          in
          (* - get the previous bucket local  stats *)
          let (old_count, old_stats) = distrib.(bucket_number) in
          (* - compute the new local stats according to the metadata   *)
          let new_loc_stats = Metadata.update_local_stats old_stats metadata in
          (* update the bucket with the new local stats *)
          distrib.(bucket_number) <- (old_count + 1, new_loc_stats) ;
          (* return the distrib and the updated global stats *)
          (distrib, Metadata.update_stats live_stats metadata))
        (distrib, Metadata.init_live_stats set.stats)
        set.set
    in
    (distrib, live_stats)

  let compute_distrib ?buckets_count set =
    let ref_point = Metadata.current_reference () in
    compute_distrib_internal ?buckets_count set ref_point
    [@@ocaml.warning "-32"]

  let resize name size =
    let len = String.length name in
    if len < size then String.concat "" [name; String.make (size - len) ' ']
    else String.sub name 0 size

  let pp_distrib_internal ~name_size ppf (t, ref_point, distrib, live_stats) =
    Format.fprintf
      ppf
      "@[History of %s %a: %a@]"
      (resize t.name name_size)
      Metadata.pp_stats
      (live_stats, ref_point)
      (fun ppf lst ->
        Format.fprintf
          ppf
          "%a"
          (Format.pp_print_list
             ~pp_sep:(fun ppf () -> Format.fprintf ppf " ; ")
             Metadata.pp_local_stats)
          lst)
      (Array.to_list distrib)

  let pp_distrib ?buckets_count ~name_size ppf t =
    let ref_point = Metadata.current_reference () in
    let (distrib, stats) =
      compute_distrib_internal ?buckets_count t ref_point
    in
    pp_distrib_internal ~name_size ppf (t, ref_point, distrib, stats)

  let pp_registered ?buckets_count ?sort () ppf () =
    if !debug then
      let ref_point = Metadata.current_reference () in
      List.iter
        (fun (section, trackers) ->
          let name_size =
            List.fold_left
              (fun size {name; _} -> max size (String.length name))
              0
              trackers
          in
          let trackers =
            let track_and_distrib =
              List.map
                (fun t ->
                  let (distrib, stats) =
                    compute_distrib_internal ?buckets_count t ref_point
                  in
                  (t, distrib, stats))
                trackers
            in
            Option.fold
              ~none:track_and_distrib
              ~some:(fun () ->
                List.sort
                  (fun (_, _, st1) (_, _, st2) -> Metadata.compare st1 st2)
                  track_and_distrib)
              sort
          in
          Format.(
            fprintf
              ppf
              "@[<v 2>%s@;@[%a@]@]"
              section
              (Format.pp_print_list
                 ~pp_sep:pp_print_cut
                 (fun ppf (t, distrib, stats) ->
                   Format.fprintf
                     ppf
                     "@[<v 2>%a@]"
                     (pp_distrib_internal ~name_size)
                     (t, ref_point, distrib, stats)))
              trackers))
        !registered
end

module SpaceTime : sig
  type t

  val start : 'a -> t

  type stats

  val init : unit -> stats

  val accumulate : stats -> t -> stats

  type reference_point

  val current_reference : unit -> reference_point

  val compute_bucket :
    t -> stats -> reference_point -> buckets_count:int -> int

  type live_stats

  val init_live_stats : stats -> live_stats

  val update_stats : live_stats -> t -> live_stats

  val compare : live_stats -> live_stats -> int

  type local_stats

  val init_local_stats : local_stats

  val update_local_stats : local_stats -> t -> local_stats

  val pp_stats : Format.formatter -> live_stats * reference_point -> unit

  val pp_local_stats : Format.formatter -> int * local_stats -> unit
end = struct
  (* tracking date and data size *)
  type t = Ptime.t * int

  (* start tracking v  *)
  let start v = (Systime_os.now (), Obj.reachable_words (Obj.repr v) * 8)

  (** stats gathered in a SpaceTime tracker.
      [init] : time of the tracker initialisation
      [alloc_size]: cummulative size aof all tracked values
      [count]: cumulative count of all tracked values
   *)
  type stats = {init : Ptime.t; alloc_size : int; alloc_count : int}

  (** We start now, with 0 allocated data  *)
  let init () = {init = Systime_os.now (); alloc_size = 0; alloc_count = 0}

  (** when adding a tracker, we add it's size to the allocated size.   *)
  let accumulate ({alloc_size; alloc_count; _} as stats) (_, size) =
    {stats with alloc_size = alloc_size + size; alloc_count = alloc_count + 1}

  let age ~now date = Ptime.diff now date

  (** Point at which we observe a set of tracked data.
   *)
  type reference_point = Ptime.t

  (** current reference is now !  *)
  let current_reference = Systime_os.now

  (** Compute the bucket of a tracked data in a distribution   *)
  let compute_bucket (creation_date, _) stats ref_point ~buckets_count : int =
    let age = age ~now:ref_point in
    let date_since_start date =
      Option.get Ptime.(Span.to_int_s (diff date stats.init))
    in
    let era = 1 + (Option.get @@ Ptime.Span.to_int_s (age stats.init)) in
    let date_since_start = date_since_start creation_date in
    let ages_duration =
      if era < buckets_count then 1 else era / buckets_count
    in
    let bucket = date_since_start / ages_duration in
    (* rounding might lead to bucket = bucket_count *)
    min bucket (buckets_count - 1)

  (** cumulative stats over live tracked data  *)
  type live_stats = {
    stats : stats;
    oldest : Ptime.t;
    total_size : int;
    biggest : int;
    count : int;
  }

  let init_live_stats stats =
    {oldest = Ptime.max; total_size = 0; biggest = 0; count = 0; stats}

  let update_stats {oldest; total_size; biggest; count; stats} (ts, sz) =
    {
      stats;
      oldest = (if Ptime.is_earlier ts ~than:oldest then ts else oldest);
      total_size = total_size + sz;
      biggest = max biggest sz;
      count = count + 1;
    }

  let compare ls1 ls2 = compare ls1.total_size ls2.total_size

  type local_stats = int

  let init_local_stats = 0

  let update_local_stats total_size (_, sz) = total_size + sz

  let pp_size ppf size =
    let div_mod i d = (i / d, i mod d) in
    let (new_size, remainder, unit) =
      let (ko, o) = div_mod size 1000 in
      if ko = 0 then (o, None, "b")
      else
        let (mo, ko) = div_mod ko 1000 in
        if mo = 0 then (ko, Some o, "Kb")
        else
          let (go, mo) = div_mod mo 1000 in
          if go = 0 then (mo, Some ko, "Mb")
          else
            let (teo, go) = div_mod go 1000 in
            if teo = 0 then (go, Some mo, "Gb") else (teo, Some go, "Tb")
    in
    Format.fprintf
      ppf
      "%d%a%s"
      new_size
      (fun ppf opt ->
        match opt with
        | None ->
            ()
        | Some dec ->
            Format.fprintf ppf ".%03d" dec)
      remainder
      unit

  let pp_span ~ref_point ppf date =
    if date = Ptime.max then Format.fprintf ppf "None"
    else Format.fprintf ppf "%a" Ptime.Span.pp (Ptime.diff ref_point date)

  let pp_stats ppf ({stats; oldest; total_size; biggest; count}, ref_point) =
    let {init; alloc_size; alloc_count} = stats in
    Format.fprintf
      ppf
      "@[<h> since %a, oldest %a, max %a, avg %a, (%#d live:%a/ %#d \
       created:%a):@]"
      (pp_span ~ref_point)
      init
      (pp_span ~ref_point)
      oldest
      pp_size
      biggest
      pp_size
      (if count > 0 then total_size / count else 0)
      count
      pp_size
      total_size
      alloc_count
      pp_size
      alloc_size

  let pp_local_stats ppf (count, local_stats) =
    Format.fprintf ppf "%d:%a" count pp_size local_stats
end

module LifeTime_Sized_Multiset = Tracker (PackedWeakMultiset) (SpaceTime)
module LifeTime_Sized_Set = Tracker (PackedWeakHashSet) (SpaceTime)

module StoryOfMyLife = struct
  (* FIXME: Could be rewritten using WeakPtr *)

  type event = string

  let create_event s = s

  type record = {order : int; values : (int, Ptime.t) Stdlib.Hashtbl.t}

  type t = {
    name : string;
    table : (string, record) Stdlib.Hashtbl.t;
    init : (int, Ptime.t) Stdlib.Hashtbl.t;
    pred_event : (string, string option) Stdlib.Hashtbl.t;
    description : string option;
    dump : bool;
    pp : bool;
  }

  let get_min_avg_max reference values =
    let max = ref Ptime.Span.zero in
    let min = ref Ptime.Span.zero in
    let sum =
      Stdlib.Hashtbl.fold
        (fun id time acc ->
          let span = Ptime.diff time (Stdlib.Hashtbl.find reference id) in
          if Ptime.Span.compare span !min < 0 then min := span ;
          if Ptime.Span.compare span !max > 0 then max := span ;
          Ptime.Span.add span acc)
        values
        Ptime.Span.zero
    in
    let sum' = Ptime.Span.to_float_s sum in
    let n = Stdlib.Hashtbl.length values in
    ( n,
      Ptime.Span.to_float_s !min,
      (if n = 0 then 0. else sum' /. float_of_int n),
      Ptime.Span.to_float_s !max )

  let iter t ?(on_failure = fun _ -> ()) f =
    let l = ref [] in
    Stdlib.Hashtbl.iter
      (fun step {order; values} -> l := (order, step, values) :: !l)
      t.table ;
    let sorted =
      List.sort (fun (order, _, _) (order', _, _) -> compare order order') !l
    in
    let pred_event = ref "" in
    List.iter
      (fun (order, step, values) ->
        try
          let reference =
            if order = 0 then t.init
            else (Stdlib.Hashtbl.find t.table !pred_event).values
          in
          let (total, min, avg, max) = get_min_avg_max reference values in
          pred_event := step ;
          f t.name step total min avg max
        with Not_found -> on_failure ())
      sorted

  let pp fmt t =
    Format.fprintf fmt "Life story of %s@." t.name ;
    let on_failure () =
      Format.fprintf
        fmt
        "Something wrong, id provided should be stable through time@."
    in
    iter t ~on_failure (fun _name step total min avg max ->
        Format.fprintf
          fmt
          "When %s: (total: %d) (min: %f) (avg: %f) (max: %f)@."
          step
          total
          min
          avg
          max)

  let last_event = ref None

  let register_event t event =
    let order = Stdlib.Hashtbl.length t.table in
    let values = Stdlib.Hashtbl.create 5 in
    Stdlib.Hashtbl.add t.table event {order; values} ;
    Stdlib.Hashtbl.add t.pred_event event !last_event ;
    (* dump *)
    Reporter.report_plots
      Reporter.default_reporter
      ~sections:([t.name], event)
      ?description:t.description
      ~plots:
        [ {
            plotname = "total";
            y_label = "number";
            curves_name = ["total"];
            plot_kind = `Normal;
          };
          {
            plotname = "stats";
            y_label = "Lifeime (s)";
            curves_name = ["min"; "avg"; "max"];
            plot_kind = `Normal;
          } ]
      ~callback:(fun () ->
        let {values; _} = Stdlib.Hashtbl.find t.table event in
        let reference =
          match Stdlib.Hashtbl.find t.pred_event event with
          | None ->
              t.init
          | Some pred_event ->
              (Stdlib.Hashtbl.find t.table pred_event).values
        in
        let (t, min, avg, max) = get_min_avg_max reference values in
        [float_of_int t; min; avg; max]) ;
    last_event := Some event

  let create ?(dump = true) ?(pp = true) ~name ?description events =
    let t =
      {
        name;
        table = Stdlib.Hashtbl.create 11;
        init = Stdlib.Hashtbl.create 5;
        pred_event = Stdlib.Hashtbl.create 11;
        description;
        dump;
        pp;
      }
    in
    last_event := None ;
    List.iter (register_event t) events ;
    t

  let milestone t event ~id () =
    if !debug then
      let {values; _} = Stdlib.Hashtbl.find t.table event in
      let id = Stdlib.Hashtbl.hash id in
      let now = Systime_os.now () in
      Stdlib.Hashtbl.replace values id now

  let emerge t ~id () =
    if !debug then
      let id = Stdlib.Hashtbl.hash id in
      let now = Systime_os.now () in
      if not (Stdlib.Hashtbl.mem t.init id) then
        Stdlib.Hashtbl.replace t.init id now
end
