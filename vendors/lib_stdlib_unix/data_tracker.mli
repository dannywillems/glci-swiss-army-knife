(** All the function belows are noop if `debug` is false *)
val debug : bool ref

module type Tracker = sig
  type t

  val create : ?register:unit -> ?section:string -> ?name:string -> unit -> t

  val track : t -> 'a -> unit

  val tracked : t -> int

  val pp_distrib :
    ?buckets_count:int -> name_size:int -> Format.formatter -> t -> unit

  val register : section:string -> t -> unit

  val unregister : t -> unit

  val pp_registered :
    ?buckets_count:int ->
    ?sort:unit ->
    unit ->
    Format.formatter ->
    unit ->
    unit
end

(** Tracking Lifetime and Size of values.
    Values added multiple time are counted multiple time.
 *)
module LifeTime_Sized_Multiset : Tracker

(** Tracking Lifetime and Size of values.

    same as LifeTime_Sized_Multiset but vaules are counted only once.
    Their might be a non-negligible computation overhead on [track]
   *)
module LifeTime_Sized_Set : Tracker

module StoryOfMyLife : sig
  type t

  type event

  val create_event : string -> event

  val create :
    ?dump:bool ->
    ?pp:bool ->
    name:string ->
    ?description:string ->
    event list ->
    t

  val emerge : t -> id:'a -> unit -> unit

  val milestone : t -> event -> id:'a -> unit -> unit

  val pp : Format.formatter -> t -> unit
end
