(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let debug = ref false

[@@@ocaml.warning "-32"]

module PackedWeakMultiset : sig
  (** multiset of weakly pointed 'a associated to 'metadata  *)
  type 'metadata t

  type packed_key = Pack_k : 'a -> packed_key [@@inlined]

  (** handle to get and remove an added value  *)
  type handle

  (** [create ~increment ~size ()] Create a set of initial size [size] that will grow by steps of [increment]   *)
  val create : ?increment:int -> ?size:int -> unit -> 'b t

  (** [add set k metadata] add to the set a weak pointer to [k] associated to [metadata]  *)
  val add : 'b t -> 'a -> 'b -> handle

  (** [remove set k] remove the weak pointer to [k] from [set] O(n).
      No-op if the element is not in the set (removed).
   *)
  val remove : 'b t -> 'a -> unit

  (** [remove set handle] remove the weak pointer added with this [handle] from [set] O(1).
      No-op if the element is not in the set (removed or not alive anymore).
  *)
  val remove_by_handle : 'b t -> handle -> unit

  (** iter over the live elements registered in the set  *)
  val iteri : (handle -> packed_key -> 'b -> unit) -> 'b t -> unit

  (** fold f init set : fold over the set with [f acc k metadata].   *)
  val fold : (packed_key -> 'metadata -> 'a -> 'a) -> 'a -> 'metadata t -> 'a

  val size : 'b t -> int

  val to_list : 'b t -> (packed_key * 'b) list

  val to_list_data : 'b t -> 'b list

  val to_list_key : 'b t -> packed_key list
end = struct
  module Eph = Ephemeron.K1

  type 'metadata pack = Pack : ('a, 'metadata) Eph.t -> 'metadata pack
  [@@inlined]

  type packed_key = Pack_k : 'a -> packed_key [@@inlined]

  type 'metadata t = {
    mutable table : 'metadata pack array;
    mutable offset : int;
    increment : int;
  }

  type handle = {mutable pos : int option}

  let create ?(increment = 1000) ?(size = 1000) () =
    {
      table = Array.init size (fun _ -> Pack (Eph.create ()));
      offset = 0;
      increment;
    }

  let grow set =
    let old_table = set.table in
    let size = Array.length old_table in
    let new_table =
      Array.init (size + set.increment) (fun i ->
          if i < size then old_table.(i) else Pack (Eph.create ()))
    in
    set.table <- new_table ;
    set.offset <- size

  let scan_free_slot set =
    let length = Array.length set.table in
    let rec loop pos table =
      if pos < length then
        let (Pack eph) = table.(pos) in
        match Eph.get_key eph with
        | None ->
            pos
        | Some _ ->
            loop (pos + 1) table
      else (grow set ; set.offset)
    in
    loop 0 set.table

  let get_slot set =
    if set.offset < Array.length set.table then (
      let res = set.offset in
      set.offset <- set.offset + 1 ;
      res )
    else scan_free_slot set

  let add set data metadata =
    let pos = get_slot set in
    let eph = Eph.create () in
    set.table.(pos) <- Pack eph ;
    Eph.set_key eph data ;
    Eph.set_data eph metadata ;
    {pos = Some pos}

  let remove set data =
    let length = Array.length set.table in
    let rec loop pos table =
      if pos < length then
        let (Pack eph) = table.(pos) in
        match Eph.get_key eph with
        | Some data' when Pack_k data = Pack_k data' ->
            Eph.unset_key eph ; Eph.unset_data eph
        | _ ->
            loop (pos + 1) table
    in
    loop 0 set.table

  let remove_by_handle set handle =
    match handle.pos with
    | Some pos ->
        let (Pack eph) = set.table.(pos) in
        Eph.unset_key eph ; Eph.unset_data eph
    | _ ->
        ()

  let iteri f set =
    let size = Array.length set.table in
    let rec loop pos =
      if pos < size then
        let (Pack eph) = set.table.(pos) in
        match (Eph.get_key eph, Eph.get_data eph) with
        | (Some key, Some data) ->
            f {pos = Some pos} (Pack_k key) data ;
            loop (pos + 1)
        | (Some _, None) ->
            assert false
        | _ ->
            loop (pos + 1)
    in
    loop 0

  let fold (f : packed_key -> 'metadata -> 'a -> 'a) (init : 'a)
      (set : 'metadata t) =
    let size = Array.length set.table in
    let rec loop pos res =
      if pos < size then
        let (Pack eph) = set.table.(pos) in
        match (Eph.get_key eph, Eph.get_data eph) with
        | (Some key, Some data) ->
            loop (pos + 1) (f (Pack_k key) data res)
        | (Some _, None) ->
            assert false
        | _ ->
            loop (pos + 1) res
      else res
    in
    loop 0 init

  let size set = fold (fun _ _ size -> size + 1) 0 set

  let to_list t = fold (fun k d l -> (k, d) :: l) [] t

  let to_list_data t = fold (fun _ d l -> d :: l) [] t

  let to_list_key t = fold (fun k _ l -> k :: l) [] t
end

module PackedWeakHashSet : sig
  (** Set of weakly pointed 'a associated to 'metadata  *)
  type 'metadata t

  type handle

  type packed_key = Pack_k : 'a -> packed_key [@@inlined]

  (** [create ~increment ~size ()] Create a set of initial size
  [size]. [increment] is ignored but allow compatibility with Multisets   *)
  val create : ?increment:int -> ?size:int -> unit -> 'metadata t

  (**  Set the merge policy of a set, used to merge metadata when a value is added twice   *)
  val set_merge_policy :
    (old:'metadata -> fresh:'metadata -> 'metadata) -> 'metadata t -> unit

  (** [add set k metadata] add to the set a weak pointer to [k] associated to [metadata]  *)
  val add : 'metadata t -> 'a -> 'metadata -> handle

  (** [remove set k] remove the weak pointer to [k] from [set] O(n).
      No-op if the element is not in the set (removed).
   *)
  val remove : 'metadata t -> 'a -> unit

  (** fold f init set : fold over the set with [f acc k metadata].   *)
  val fold : (packed_key -> 'metadata -> 'a -> 'a) -> 'a -> 'metadata t -> 'a

  val size : 'metadata t -> int

  val to_list : 'metadata t -> (packed_key * 'metadata) list

  val to_list_data : 'metadata t -> 'metadata list

  val to_list_key : 'metadata t -> packed_key list
end = struct
  module Eph = Ephemeron.K1

  type handle = unit

  type 'metadata pack = Pack : ('a, 'metadata) Eph.t -> 'metadata pack
  [@@inlined]

  type packed_key = Pack_k : 'a -> packed_key [@@inlined]

  module Eph_Hashset = Ephemeron.GenHashTable.MakeSeeded (struct
    type t = packed_key

    type 'metadata container = 'metadata pack

    let equal (Pack eph) (Pack_k k) =
      Ephemeron.GenHashTable.(
        Option.fold ~none:EDead ~some:(fun orig_k ->
            if orig_k = Obj.magic k then ETrue else EFalse))
        (Eph.get_key eph)

    let hash seed (Pack_k k) = Hashtbl.seeded_hash seed k

    let create (Pack_k k) metadata =
      let eph = Eph.create () in
      Eph.set_key eph k ; Eph.set_data eph metadata ; Pack eph

    let get_key (Pack eph) = Option.map (fun k -> Pack_k k) (Eph.get_key eph)

    let get_data (Pack eph) = Eph.get_data eph

    let set_key_data (Pack eph) (Pack_k k) metadata =
      Eph.set_key eph (Obj.magic k) ;
      Eph.set_data eph metadata

    let check_key (Pack eph) = Eph.check_key eph
  end)

  type 'metadata t = {
    set : 'metadata Eph_Hashset.t;
    mutable merge : old:'metadata -> fresh:'metadata -> 'metadata;
  }

  let set_merge_policy (policy : old:'metadata -> fresh:'metadata -> 'metadata)
      (set : 'metadata t) =
    set.merge <- policy

  let create ?(increment = 1000) ?(size = 1000) () =
    ignore increment ;
    {
      set = Eph_Hashset.create size;
      merge = (fun ~old ~fresh -> ignore old ; fresh);
    }

  let add {set; merge} data metadata =
    let data = Pack_k data in
    match Eph_Hashset.find_opt set data with
    | None ->
        Eph_Hashset.add set data metadata
    | Some old ->
        Eph_Hashset.add set data (merge ~old ~fresh:metadata)

  let remove set data = Eph_Hashset.remove set.set (Pack_k data)

  let fold f init set = Eph_Hashset.fold f set.set init

  let size set = fold (fun _ _ size -> size + 1) 0 set

  let to_list t = fold (fun k d l -> (k, d) :: l) [] t

  let to_list_data t = fold (fun _ d l -> d :: l) [] t

  let to_list_key t = fold (fun k _ l -> k :: l) [] t
end

[@@@ocaml.warning "+32"]
